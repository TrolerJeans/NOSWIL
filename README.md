# NOSWIL

This is a Lisp operating system. NOSWIL is an operating system which uses Lisp's interactive environs as [the main source of inspiration](http://metamodular.com/lispos.pdf).

This is not meant to be a fully working OS. I just wanted to extend my Common Lisp knowledge.

Version: 00-McCarthy